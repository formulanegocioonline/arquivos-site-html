/* TRABALHAR COM DATAS DO MÊS */
var d = new Date();
var month = new Array();
month[0] = "Janeiro";
month[1] = "Fevereiro";
month[2] = "Março";
month[3] = "Abril";
month[4] = "Maio";
month[5] = "Junho";
month[6] = "Julho";
month[7] = "Agosto";
month[8] = "Setembro";
month[9] = "Outubro";
month[10] = "Novembro";
month[11] = "Dezembro";
var n = month[d.getMonth()]; /* Pega mês atual */
diasMesHoje = d.getDate();
diasMesOntem = d.getDate()-1;
diasMesAnteontem = d.getDate()-2;
dataMesAtual = n + ' de ' + d.getFullYear();

/* Editado
diasMes = d.getDate()-2 + '' + d.getDate()-1 + ' e ' + d.getDate() + ' de ' + n;
*/

/* CÓDIGO ORIGINAL

dataHora = d.getDate() + ' de ' + n + ' de ' + d.getFullYear();
dataHoraFinal = d.getDate() + ' de ' + n + ' de ' + d.getFullYear();
dataHoraLateral = d.getDate() + ' de ' + n + ' de ' + d.getFullYear(); 
*/

document.getElementById("datahoje").innerHTML = diasMesAnteontem+", "+diasMesOntem+" e "+diasMesHoje +' de '+ n +'(Isso, termina HOJE!)';
document.getElementById("mesAtual").innerHTML = dataMesAtual;

/* MOSTRAR HORA ATUAL EXATA
var relogio = document.getElementById('relogioReal');
setInterval(function() {
    relogio.innerHTML = ((new Date).toLocaleString().substr(11, 8));
}, 1000); */